package com.nttdata.jjla.cuentabancariaservice.repository;

import com.nttdata.jjla.cuentabancariaservice.entity.CuentaBancaria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CuentaBancariaRepository  extends JpaRepository<CuentaBancaria, Long> {
}
