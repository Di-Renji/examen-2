package com.nttdata.jjla.cuentabancariaservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CuentaBancariaDto {

    private Long id;
    private Integer nro_cuenta;
    private String tipo_cuenta;
    private Boolean active;
    private Long bancoId;
    private Long clienteId;

}
