package com.nttdata.jjla.cuentabancariaservice.controller;

import com.nttdata.jjla.cuentabancariaservice.dto.CuentaBancariaDto;
import com.nttdata.jjla.cuentabancariaservice.service.CuentaBancariaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cuenta")
public class CuentaBancariaController {

    @Autowired
    private CuentaBancariaService cuentaBancariaService;
    @PostMapping("/registrar")
    public ResponseEntity<CuentaBancariaDto> registrar(@RequestBody CuentaBancariaDto bancoDto){
        return new ResponseEntity<>(cuentaBancariaService.registrar(bancoDto), HttpStatus.CREATED);
    }
    @GetMapping("/listar")
    public List<CuentaBancariaDto> listar(){
        return cuentaBancariaService.listar();
    }

    @PutMapping("actualizar/{id}")
    public ResponseEntity<CuentaBancariaDto> actualizar(@RequestBody CuentaBancariaDto bancoDto, @PathVariable(name = "id") long id){
        CuentaBancariaDto bancoRespuesta = cuentaBancariaService.actualizar(bancoDto, id);
        return new ResponseEntity<>(bancoRespuesta, HttpStatus.OK);
    }

    @DeleteMapping("eliminar/{id}")
    public ResponseEntity<String> eliminar(@PathVariable(name = "id") long id){
        cuentaBancariaService.eliminar(id);
        return new ResponseEntity<>("Eliminada con exito", HttpStatus.OK);
    }


}
