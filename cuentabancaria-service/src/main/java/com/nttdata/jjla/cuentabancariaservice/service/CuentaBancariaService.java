package com.nttdata.jjla.cuentabancariaservice.service;

import com.nttdata.jjla.cuentabancariaservice.dto.CuentaBancariaDto;
import com.nttdata.jjla.cuentabancariaservice.entity.CuentaBancaria;
import com.nttdata.jjla.cuentabancariaservice.repository.CuentaBancariaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CuentaBancariaService implements ICuentaService {

    @Autowired
    private CuentaBancariaRepository cuentaBancariaRepository;
    @Autowired
    public ModelMapper modelMapper;

    @Override
    public CuentaBancariaDto registrar(CuentaBancariaDto cuentaBancariaDto) {
        CuentaBancaria banco = mapearCuentaBancaria(cuentaBancariaDto);
        CuentaBancaria bancoRegistrado = cuentaBancariaRepository.save(banco);
        CuentaBancariaDto bancoRespuesta = mapearCuentaBancariaDTO(bancoRegistrado);
        return bancoRespuesta;
    }

    @Override
    public List<CuentaBancariaDto> listar() {
        List<CuentaBancaria> bancos= cuentaBancariaRepository.findAll();
        return bancos.stream().map(cuentaBancaria -> mapearCuentaBancariaDTO(cuentaBancaria)).collect(Collectors.toList());
    }

    @Override
    public CuentaBancariaDto actualizar(CuentaBancariaDto cuentaBancariaDto, long id) {
        return null;
    }

    @Override
    public void eliminar(long id) {

    }

    /*************** MAPEO CON MODELMAPPER *******************/
    private CuentaBancaria mapearCuentaBancaria(CuentaBancariaDto cuentaBancariaDto){
        CuentaBancaria cuentaBancaria = modelMapper.map(cuentaBancariaDto, CuentaBancaria.class);
        return cuentaBancaria;
    }

    private CuentaBancariaDto mapearCuentaBancariaDTO( CuentaBancaria cuentaBancaria){
        CuentaBancariaDto cuentaBancariaDto = modelMapper.map(cuentaBancaria,CuentaBancariaDto.class);
        return cuentaBancariaDto;
    }
}
