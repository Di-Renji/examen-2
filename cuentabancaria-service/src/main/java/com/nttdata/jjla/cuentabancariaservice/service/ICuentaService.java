package com.nttdata.jjla.cuentabancariaservice.service;

import com.nttdata.jjla.cuentabancariaservice.dto.CuentaBancariaDto;

import java.util.List;

public interface ICuentaService {

    CuentaBancariaDto registrar(CuentaBancariaDto cuentaBancariaDto);
    List<CuentaBancariaDto> listar();
    CuentaBancariaDto actualizar(CuentaBancariaDto cuentaBancariaDto, long id);
    void eliminar(long id);

}
