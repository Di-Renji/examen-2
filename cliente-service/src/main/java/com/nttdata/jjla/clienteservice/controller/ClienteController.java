package com.nttdata.jjla.clienteservice.controller;

import com.nttdata.jjla.clienteservice.dto.ClienteDto;
import com.nttdata.jjla.clienteservice.service.ClienteServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cliente")
public class ClienteController {
    @Autowired
    private ClienteServiceImpl clienteService;

    @PostMapping("/registrar")
    public ResponseEntity<ClienteDto> registrar(@RequestBody ClienteDto clienteDto){
        return new ResponseEntity<>(clienteService.registrar(clienteDto), HttpStatus.CREATED);
    }
    @GetMapping("/listar")
    public List<ClienteDto> listar(){
        return clienteService.listar();
    }

    @PutMapping("actualizar/{id}")
    public ResponseEntity<ClienteDto> actualizar(@RequestBody ClienteDto clienteDto, @PathVariable(name = "id") long id){
        ClienteDto bancoRespuesta = clienteService.actualizar(clienteDto, id);
        return new ResponseEntity<>(bancoRespuesta, HttpStatus.OK);
    }

    @DeleteMapping("eliminar/{id}")
    public ResponseEntity<String> eliminar(@PathVariable(name = "id") long id){
        clienteService.eliminar(id);
        return new ResponseEntity<>("Eliminada con exito", HttpStatus.OK);
    }
}
