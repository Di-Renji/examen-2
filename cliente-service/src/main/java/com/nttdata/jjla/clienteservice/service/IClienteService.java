package com.nttdata.jjla.clienteservice.service;

import com.nttdata.jjla.clienteservice.dto.ClienteDto;

import java.util.List;

public interface IClienteService {

    ClienteDto registrar(ClienteDto clienteDto);
    List<ClienteDto> listar();
    ClienteDto actualizar(ClienteDto clienteDto, long id);
    void eliminar(long id);

}
