package com.nttdata.jjla.clienteservice.service;

import com.nttdata.jjla.clienteservice.dto.ClienteDto;
import com.nttdata.jjla.clienteservice.entity.Cliente;
import com.nttdata.jjla.clienteservice.modelos.CuentaBancaria;
import com.nttdata.jjla.clienteservice.repository.ClienteRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.lang.module.ResolutionException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClienteServiceImpl implements IClienteService {

    @Autowired
    public ModelMapper modelMapper;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ClienteRepository clienteRepository;

    public List<CuentaBancaria> listarCuentas(Long idCuenta){
        List<CuentaBancaria> cuentaBancarias = restTemplate.getForObject("http://localhost:8005/api/cuenta/cliente"+idCuenta, List.class);
        return cuentaBancarias;
    }

    @Override
    public ClienteDto registrar(ClienteDto clienteDto) {
        Cliente banco = mapearCliente(clienteDto);
        Cliente bancoRegistrado = clienteRepository.save(banco);
        ClienteDto bancoRespuesta = mapearClienteDTO(bancoRegistrado);
        return bancoRespuesta;
    }

    @Override
    public List<ClienteDto> listar() {
        List<Cliente> bancos= clienteRepository.findAll();
        return bancos.stream().map(publicacion -> mapearClienteDTO(publicacion)).collect(Collectors.toList());
    }

    @Override
    public ClienteDto actualizar(ClienteDto clienteDto, long id) {
        return null;
    }

    @Override
    public void eliminar(long id) {
        Cliente banco = clienteRepository.findById(id).orElseThrow(()->new ResolutionException("Recurso No Encontrado"));
        clienteRepository.delete(banco);
    }

    /*************** MAPEO CON MODELMAPPER *******************/
    private Cliente mapearCliente(ClienteDto clienteDto){
        Cliente cliente = modelMapper.map(clienteDto, Cliente.class);
        return cliente;
    }

    private ClienteDto mapearClienteDTO(Cliente cliente){
        ClienteDto clienteDto = modelMapper.map(cliente,ClienteDto.class);
        return clienteDto;
    }

}
