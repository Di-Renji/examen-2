package com.nttdata.jjla.bancoservice.service;

import com.nttdata.jjla.bancoservice.dto.BancoDto;

import java.util.List;

public interface IBancoService {

    BancoDto registrar(BancoDto bancoDto);
    List<BancoDto> listar();
    BancoDto actualizar(BancoDto bancoDto, long id);
    void eliminar(long id);

}
