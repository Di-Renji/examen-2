package com.nttdata.jjla.bancoservice.controller;

import com.nttdata.jjla.bancoservice.dto.BancoDto;
import com.nttdata.jjla.bancoservice.service.BancoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/banco")
public class BancoController {

    @Autowired
    private BancoServiceImpl bancoService;

    @PostMapping("/registrar")
    public ResponseEntity<BancoDto> registrar(@RequestBody BancoDto bancoDto){
        return new ResponseEntity<>(bancoService.registrar(bancoDto), HttpStatus.CREATED);
    }
    @GetMapping("/listar")
    public List<BancoDto> listar(){
        return bancoService.listar();
    }

    @PutMapping("actualizar/{id}")
    public ResponseEntity<BancoDto> actualizar(@RequestBody BancoDto bancoDto, @PathVariable(name = "id") long id){
        BancoDto bancoRespuesta = bancoService.actualizar(bancoDto, id);
        return new ResponseEntity<>(bancoRespuesta, HttpStatus.OK);
    }

    @DeleteMapping("eliminar/{id}")
    public ResponseEntity<String> eliminar(@PathVariable(name = "id") long id){
        bancoService.eliminar(id);
        return new ResponseEntity<>("Eliminada con exito", HttpStatus.OK);
    }

}
