package com.nttdata.jjla.bancoservice.service;

import com.nttdata.jjla.bancoservice.dto.BancoDto;
import com.nttdata.jjla.bancoservice.entity.Banco;
import com.nttdata.jjla.bancoservice.modelos.CuentaBancaria;
import com.nttdata.jjla.bancoservice.repository.BancoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.lang.module.ResolutionException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BancoServiceImpl implements IBancoService {

    @Autowired
    private BancoRepository bancoRepository;
    @Autowired
    public ModelMapper modelMapper;
    @Autowired
    private RestTemplate restTemplate;

    public List<CuentaBancaria> listarCuentas(Long idCuenta){
        List<CuentaBancaria> cuentaBancarias = restTemplate.getForObject("http://localhost:8005/api/cuenta/banco"+idCuenta, List.class);
        return cuentaBancarias;
    }

    @Override
    public BancoDto registrar(BancoDto bancoDto) {
        Banco banco = mapearBanco(bancoDto);
        Banco bancoRegistrado = bancoRepository.save(banco);
        BancoDto bancoRespuesta = mapearBancoDTO(bancoRegistrado);
        return bancoRespuesta;
    }

    @Override
    public List<BancoDto> listar() {
        List<Banco> bancos= bancoRepository.findAll();
        return bancos.stream().map(publicacion -> mapearBancoDTO(publicacion)).collect(Collectors.toList());
    }

    @Override
    public BancoDto actualizar(BancoDto bancoDto, long id) {
        Banco banco = bancoRepository.findById(id).orElseThrow(()-> new ResolutionException("Recurso No Encontrado"));
        banco.setNombre(bancoDto.getNombre());
        banco.setDireccion(bancoDto.getDireccion());
        banco.setActive(bancoDto.getActive());
        Banco bancoActualizado = bancoRepository.save(banco);
        return mapearBancoDTO(bancoActualizado);
    }

    @Override
    public void eliminar(long id) {
        Banco banco = bancoRepository.findById(id).orElseThrow(()->new ResolutionException("Recurso No Encontrado"));
        bancoRepository.delete(banco);
    }

    /*************** MAPEO CON MODELMAPPER *******************/
    private Banco mapearBanco(BancoDto bancoDto){
        Banco banco = modelMapper.map(bancoDto, Banco.class);
        return banco;
    }

    private BancoDto mapearBancoDTO(Banco banco){
        BancoDto bancoDto = modelMapper.map(banco,BancoDto.class);
        return bancoDto;
    }

}
