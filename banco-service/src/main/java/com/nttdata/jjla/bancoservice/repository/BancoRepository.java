package com.nttdata.jjla.bancoservice.repository;

import com.nttdata.jjla.bancoservice.entity.Banco;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BancoRepository extends JpaRepository<Banco, Long> {
}
