package com.nttdata.jjla.bancoservice.modelos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CuentaBancaria {

    private Integer nro_cuenta;
    private String tipo_cuenta;
    private Boolean active;
    private Long bancoId;

}
